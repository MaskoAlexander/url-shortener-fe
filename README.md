# URL Shortener FE

## Pre-requisites
| Name | Version |
| ------ | ------ |
| Docker | v 18.09.0^ |
| docker-compose | v 1.23.0^ |

## Installation

Get a copy of the source code

```sh
$ mkdir \var\www\url-shortener-fe
$ cd !$
$ git clone https://MaskoAlexander@bitbucket.org/MaskoAlexander/url-shortener-fe.git
$ git checkout -b master origin/master
```

Create env gitignored file and fill it with corresponding variables
```sh
$ cp env.js.example env.js
```

## Running on the local machine

Run Docker container
```sh
$ docker-compose up
```

In a case of any update or unexpected issues rebuild container
```sh
$ docker-compose up --build
```

Verify the deployment by navigating to your server address in your preferred browser.

[http://127.0.0.1:8000//](http://127.0.0.1:8000/)
