# Install node
FROM node:8.10

# Set the workdir /var/www/fe
WORKDIR /fe

# Copy the package.json to workdir
COPY package.json ./

# Run npm install - install the npm dependencies
RUN npm install
RUN npm install local-web-server -g

# Copy application source
COPY . .

# Start the application
CMD ["ws", "--spa", "index.html"]