$(() => {
	let url           = new URL(window.location.href);
	const shorten_url = url.pathname.replace(/\//g, '');

	// Sort of primitive but fast routing
	if (shorten_url) {
		// Make a call to service and redirect
		redirectToExpandedLink(shorten_url)
	} else {
		//Init required homepage events
		initEvents();
	}
});

/**
 * Initialize homepage events
 */
const initEvents = () => {
	$('#js-shorten-action').on('click', () => {
		$('#js-copy-to-clipboard').text('Copy to clipboard');
		const url = $('#js-url-input').val();

		if (!url) {
			alert('Please enter the URL before submit');
			return;
		}

		// Use build in HTML5 url validation
		if (!$('#js-url-input')[0].checkValidity()) {
			alert('Not a valid URL');
			return;
		}

		$.ajax(env.SHORTENER_SERVICE_BASE_URL + '/api/v1/shorten', {
			data: JSON.stringify({extended: url}),
			contentType: 'application/json',
			type: 'POST',
			success: (res) => {
				showShortenLink(res.short);
			},
			error: (request) => {
				const message = request.responseJSON.message;

				alert(message);
			}
		});
	});

	$('#js-new-tab-open').on('click', () => {
		window.open($('#shot-link-text-js').text(), '_blank');
	});

	$('#js-copy-to-clipboard').on('click', () => {
		$('#js-copy-buffer').val($('#shot-link-text-js').text()).removeClass('hidden').select();

		document.execCommand("copy");
		$('#js-copy-buffer').addClass('hidden');
		$('#js-copy-to-clipboard').text('Copied!');
	});
};

/**
 * Redirect to expanded link
 * @param {string} shorten_url
 */
const redirectToExpandedLink = (shorten_url) => {
	$.ajax(env.SHORTENER_SERVICE_BASE_URL + '/api/v1/extend/' + shorten_url, {
		success: (res) => {
			// Redirect to the extended(full) URL
			window.location.replace(res.extended);
		},
		error: (request) => {
			const message = request.responseJSON.message;

			alert(message);
			// Redirect to the homepage
			document.location.href = "/";
		}
	});
};

/**
 * Show shorten link
 * @param {string} short_id
 */
const showShortenLink = (short_id) => {
	const link = window.location.protocol + '//' + window.location.host + '/' + short_id;

	$('#shot-link-text-js').text(link);
	$('.message-box').removeClass('hidden');
};
